import styled from "styled-components";

export const Item = {
  Main: styled.div`
    display: flex;
    align-items: center;
    padding: 0 15px;
    justify-content: space-between;
    border: 1px solid #d3d7db;
    filter: drop-shadow(0px 7px 64px rgba(0, 0, 0, 0.07));
    border-radius: 6px;
    .selected {
      color: #004dff;
      background: #bbcffb;
      padding: 6px 12px;
      border-radius: 6px;
    }
  `,
  Number: styled.p`
    border-radius: 50%;
    width: 24px;
    height: 24px;
    border: 1px solid #004dff;
    color: #004dff;
  `,
  Text: styled.p`
    cursor: pointer;
  `,
};

export const Wrapper = {
  Main: styled.div`
    display: flex;
    flex-direction: column;
    gap: 15px;
  `,
  Next: styled.div`
    .disable {
      background: #004dff8c;
    }
    > button {
      width: 100%;
      height: 56px;
      border: none;
      background: #004dff;
      border-radius: 10px;
      font-weight: 700;
      font-size: 17px;
      line-height: 100%;
      margin: 20px 0;
      cursor: pointer;
      text-align: center;
      text-transform: uppercase;

      color: #ffffff;
    }
  `,
  Work: styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    > p {
      font-style: normal;
      font-weight: 500;
      font-size: 17px;
      line-height: 23px;

      display: flex;
      align-items: center;

      color: #04004d;
    }
    > img {
      cursor: pointer;
    }
  `,
};
export const Phrase = {
  Main: styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
  `,
  Amount: styled.div``,
  Text: styled.p`
    font-style: normal;
    font-weight: 500;
    font-size: 17px;
    line-height: 23px;

    display: flex;
    align-items: center;

    color: #004dff;
  `,
};
export const Wrong = styled.div`
  display: flex;
  gap: 10px;
  > p {
    color: #ff0366;
  }
`;
