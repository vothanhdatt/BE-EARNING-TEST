import Create from "components/Create";
import SVG from "components/SVG";
import { useState } from "react";
import { ClipLoader } from "react-spinners";
import { toast } from "react-toastify";
import { checkKeyword } from "utits/json";
import { Item, Phrase, Wrapper, Wrong } from "./styles";

const filterPrimary = (obj, value, primary) => {
  for (let i in obj) {
    if (obj[i] === value && i == primary) {
      return true;
    }
  }
  return false;
};
const Step2 = ({ setStep }) => {
  const [keyword, setKeyword] = useState({});
  const [show, setShow] = useState(false);
  const [err, setErr] = useState(false);
  const [loading, setLoading] = useState(false);

  const data = JSON.parse(localStorage.getItem("keyword"));
  const selected = JSON.parse(localStorage.getItem("selected"));

  const handleSelect = (primary, value) => {
    setKeyword({ ...keyword, [primary]: value });
  };

  const handleSubmit = async () => {
    setLoading(true);
    await new Promise(r => setTimeout(r, 2000));
    setLoading(false);
    if (checkKeyword(keyword, selected) && Object.keys(keyword).length === 6) {
      setShow(true);
    } else {
      toast.error("wrong!");
      setErr(true);
    }
  };
  return (
    <>
      <Phrase.Main>
        <Phrase.Text>Confirm Your Seed Phrase</Phrase.Text>
        <Phrase.Amount>{Object.keys(keyword).length}/6</Phrase.Amount>
      </Phrase.Main>
      <Wrapper.Main>
        {data?.map(item => (
          <Item.Main key={item.index}>
            <Item.Number>{item?.index}</Item.Number>
            {item?.value?.map((item1, i) => (
              <Item.Text
                key={i}
                onClick={() => handleSelect(item?.index, item1)}
                className={
                  filterPrimary(keyword, item1, item?.index) && "selected"
                }
              >
                {item1}
              </Item.Text>
            ))}
          </Item.Main>
        ))}
        {err && (
          <Wrong>
            <SVG name="Wrong"></SVG>{" "}
            <p>Wrong seed phrases. Please try again!</p>
          </Wrong>
        )}

        <Wrapper.Next>
          <Wrapper.Work>
            <p>How does this work?</p>
            <SVG name="Right" />
          </Wrapper.Work>
          <button
            disabled={loading || Object.keys(keyword).length !== 6}
            className={
              loading || (Object.keys(keyword).length !== 6 && "disable")
            }
            onClick={() => handleSubmit()}
          >
            {loading ? <ClipLoader color="#36d7b7" /> : "SUBMIT"}
          </button>
        </Wrapper.Next>
      </Wrapper.Main>
      <Create show={show} setShow={setShow} setStep={setStep} />
    </>
  );
};
export default Step2;
