import styled from "styled-components";

export const Wrapper = {
  Main: styled.div``,
  Back: styled.p`
    font-style: normal;
    font-weight: 500;
    font-size: 17px;
    line-height: 23px;
    color: #04004d;
    display: flex;
    gap: 5px;
  `,
  Auto: styled.p`
    font-style: normal;
    font-weight: 500;
    font-size: 17px;
    line-height: 23px;

    display: flex;
    align-items: center;

    color: #004dff;
  `,
  Copy: styled.div`
    display: flex;
    gap: 10px;
    align-items: center;
    margin: 20px 0;

    > p {
      font-style: normal;
      font-weight: 400;
      font-size: 15px;
      line-height: 21px;

      display: flex;
      align-items: center;

      color: #04004d;
    }
    > img {
      cursor: pointer;
    }
  `,
  Keyword: styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    gap: 10px;
  `,
  Next: styled.div`
    > button {
      width: 100%;
      height: 56px;
      border: none;
      background: #004dff;
      border-radius: 10px;
      font-weight: 700;
      font-size: 17px;
      line-height: 100%;
      cursor: pointer;

      text-align: center;
      text-transform: uppercase;

      color: #ffffff;
      margin: 20px 0;
    }
  `,
  Work: styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    > p {
      font-style: normal;
      font-weight: 500;
      font-size: 17px;
      line-height: 23px;

      display: flex;
      align-items: center;

      color: #04004d;
    }
    > img {
      cursor: pointer;
    }
  `,
};

export const Item = {
  Number: styled.div`
    width: 20px;
    height: 20px;
    background: #bbcffb;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: space-evenly;

    font-style: normal;
    font-weight: 500;
    font-size: 11px;
    line-height: 13px;

    color: #004dff;
  `,
  Text: styled.p`
    font-style: normal;
    font-weight: 400;
    font-size: 15px;
    line-height: 21px;
    color: #04004d;
  `,
  Main: styled.div`
    box-shadow: 0px 7px 32px rgba(0, 0, 0, 0.07);
    border-radius: 6px;
    display: flex;
    align-items: center;
    gap: 10px;
    justify-content: flex-start;
    padding: 0 10px;
  `,
};

export const Phrase = {
  Main: styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
  `,
  Amount: styled.div``,
  Text: styled.p`
    font-style: normal;
    font-weight: 500;
    font-size: 17px;
    line-height: 23px;

    display: flex;
    align-items: center;

    color: #004dff;
  `,
};
