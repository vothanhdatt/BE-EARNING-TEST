import SVG from "components/SVG";
import { Item, Phrase, Wrapper } from "./styles";
import { json } from "data";
import { randomKeyword, selectKeyword } from "utits/json";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { useMemo, useState } from "react";
import Copied from "components/Copied";
import { toast } from "react-toastify";

const Step1 = ({ setStep }) => {
  const newArr = useMemo(() => {
    return randomKeyword(json, 24);
  }, []);

  const [activeCopy, setActiveCopy] = useState(false);

  const handleCopy = () => {
    setActiveCopy(true);
    setTimeout(() => {
      setActiveCopy(false);
      toast.success("Copied");
    }, 2000);
  };

  const handleNext = () => {
    localStorage.setItem("keyword", JSON.stringify(selectKeyword(newArr)));
    setStep(2);
  };
  return (
    <>
      <Phrase.Main>
        <Phrase.Text>Auto Gen Seed Phrase?</Phrase.Text>
      </Phrase.Main>
      <Wrapper.Keyword>
        {newArr?.map((e, i) => (
          <Item.Main key={i}>
            <Item.Number>{e?.index}</Item.Number>
            <Item.Text>{e?.value}</Item.Text>
          </Item.Main>
        ))}
      </Wrapper.Keyword>
      <Wrapper.Copy>
        <p>
          Tap to Copy or Carefully write down your seed phrase and store it in a
          safe place
        </p>
        <CopyToClipboard text={JSON.stringify(newArr)}>
          <SVG name="Copy" onClick={() => handleCopy()} />
        </CopyToClipboard>
      </Wrapper.Copy>
      <Wrapper.Next>
        <Wrapper.Work>
          <p>How does this work?</p>
          <SVG name="Right" />
        </Wrapper.Work>
        <button onClick={() => handleNext()}>NEXT</button>
      </Wrapper.Next>
      <Copied activeCopy={activeCopy} setActiveCopy={setActiveCopy} />
    </>
  );
};
export default Step1;
