import styled from "styled-components";

export const Wrapper = {
  Main: styled.div``,
  Back: styled.p`
    font-style: normal;
    font-weight: 500;
    font-size: 17px;
    line-height: 23px;
    color: #04004d;
    display: flex;
    gap: 5px;
    cursor: pointer;
  `,
};
export const Phrase = {
  Main: styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
  `,
  Amount: styled.div``,
  Text: styled.p`
    font-style: normal;
    font-weight: 500;
    font-size: 17px;
    line-height: 23px;

    display: flex;
    align-items: center;

    color: #004dff;
  `,
};
