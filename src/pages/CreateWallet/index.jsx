import Copied from "components/Copied";
import SVG from "components/SVG";
import { useState } from "react";
import Step1 from "./Step1";
import Step2 from "./Step2";
import { Item, Phrase, Wrapper } from "./styles";

const CreateWallet = () => {
  const [step, setStep] = useState(1);
  return (
    <>
      <div className="container">
        <Wrapper.Back
          onClick={() => {
            setStep(1);
          }}
        >
          <SVG name="Left" />
          Create New Wallet
        </Wrapper.Back>

        {step === 1 && <Step1 setStep={setStep} />}
        {step === 2 && <Step2 setStep={setStep} />}
      </div>
    </>
  );
};
export default CreateWallet;
