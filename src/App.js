import "./App.css";
import { sort } from "./utits/sort";
import CreateWallet from "./pages/CreateWallet";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
function App() {
  // Cau 1:
  const arr = [0, 10, 1, 99, 9, 8, 79, 91, 22, 32, 12];
  const arr1 = [99, 19, 29, 39, 11, 21, 32, 33, 35, 50, 60, 90];
  const arr2 = [1, 10, 19, 11, 13, 16, 19];
  console.log("Array 1: ", sort(arr));
  console.log("Array 2: ", sort(arr1));
  console.log("Array 3: ", sort(arr2));
  // Cau 2, 3:
  return (
    <div className="App">
      <CreateWallet />
      <ToastContainer />
    </div>
  );
}

export default App;
