export const sort = arr => {
  const newArr = arr.sort(function (a, b) {
    return b - a;
  });

  let str = "";
  newArr.forEach(e => {
    str = str + e;
  });
  return str;
};
