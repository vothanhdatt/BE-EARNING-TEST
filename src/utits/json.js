export const randomKeyword = (arr, num) => {
  const newArr = [];

  let count = 0;

  do {
    const index = Math.floor(Math.random() * arr.length);
    if (checkValue(newArr, arr[index])) {
      newArr.push({ value: arr[index], index: count + 1 });
      count++;
    }
  } while (count < num);

  return newArr;
};

export const selectKeyword = arr => {
  spliceArr(arr, 6);

  const selected = [];
  let count = 0;

  do {
    const index = Math.floor(Math.random() * arr.length);

    if (checkIndex(selected, arr[index])) {
      selected.push({ index: arr[index].index, value: arr[index].value });
      arr.splice(arr.indexOf(arr[index]), 1);
      count++;
    }
  } while (count < 6);

  //
  localStorage.setItem("selected", JSON.stringify(selected));

  return keyword(selected, arr);
};
const keyword = (selected, arr) => {
  const newArr = [];
  //
  for (let i = 0; i < selected.length; i++) {
    let arrIndex = [];
    let count1 = 0;
    do {
      const ran = Math.floor(Math.random() * arr.length);
      if (!arrIndex.includes(ran)) {
        arrIndex.push(ran);
        count1++;
      }
    } while (count1 < 2);
    //
    swapPush(newArr, selected, arr, arrIndex, i);
    arrIndex.forEach(element => {
      arr.splice(arr.indexOf(arr[element]), 1);
    });
    arrIndex = [];
  }
  return newArr;
};

const swapPush = (newArr, selected, arr, arrIndex, i) => {
  const ran = Math.floor(Math.random() * 3) + 1;
  if (ran === 1) {
    newArr.push({
      index: selected[i].index,
      value: [
        selected[i].value,
        arr[arrIndex[0]].value,
        arr[arrIndex[1]].value,
      ],
    });
  } else if (ran === 2) {
    newArr.push({
      index: selected[i].index,
      value: [
        arr[arrIndex[0]].value,
        selected[i].value,
        arr[arrIndex[1]].value,
      ],
    });
  } else if (ran === 3) {
    newArr.push({
      index: selected[i].index,
      value: [
        arr[arrIndex[1]].value,
        arr[arrIndex[0]].value,
        selected[i].value,
      ],
    });
  }
};

const spliceArr = (arr, num) => {
  if (arr?.length > 18) {
    for (let i = 0; i < num; i++) {
      const index = Math.floor(Math.random() * arr.length);
      arr.splice(arr.indexOf(arr[index]), 1);
    }
    return arr;
  } else {
    return arr;
  }
};

const checkIndex = (arr, arr2) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i]?.index === arr2.index) return false;
  }
  return true;
};
const checkValue = (arr, value) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i]?.value === value) return false;
  }
  return true;
};

//
export const checkKeyword = (obj, arr) => {
  for (let i in obj) {
    for (let j = 0; j < arr.length; j++) {
      if (Number(i) === arr[j].index && obj[i] !== arr[j].value) {
        return false;
      }
    }
  }
  return true;
};
