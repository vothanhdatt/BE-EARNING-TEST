import SVG from "components/SVG";
import { Checkbox, Overlay } from "./styles";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

const Create = ({ show, setShow, setStep }) => {
  const [checked, setChecked] = useState({
    one: false,
    two: false,
    three: false,
  });
  const handleCheck = e => {
    const { name } = e.target;
    setChecked({ ...checked, [name]: !checked[name] });
  };

  const handleSubmit = () => {
    toast.success("Successfully!");
    setChecked({
      one: false,
      two: false,
      three: false,
    });
    setShow(false);
    setStep(1);
  };
  return (
    <>
      {show && (
        <Overlay>
          <div>
            <div className="down" onClick={() => setShow(false)}>
              <SVG name="Down" />
            </div>
            <SVG name="Success" />
            <p>Your wallet has been created!</p>
            <Checkbox.Main>
              <Checkbox.Item>
                <input
                  type="checkbox"
                  name="one"
                  id="one"
                  onClick={e => handleCheck(e)}
                />{" "}
                <label htmlFor="one">
                  Metanode cannot recover your seed phrase. You should back up
                  your seed phrase and keep it safe, it’s your responsibility.
                </label>
              </Checkbox.Item>
              <Checkbox.Item>
                <input
                  type="checkbox"
                  name="two"
                  id="two"
                  onClick={e => handleCheck(e)}
                />{" "}
                <label htmlFor="two">
                  Your transaction data is one of the most important security
                  keys which is needed for every incurred transaction. You
                  should back up your data automatically and secure back up file
                  with a strong pasword.
                </label>
              </Checkbox.Item>
              <Checkbox.Item>
                <input
                  type="checkbox"
                  name="three"
                  id="three"
                  onClick={e => handleCheck(e)}
                />{" "}
                <label htmlFor="three">
                  To keep your backup file safe, you should also keep secret
                  your back up location and secure it.
                </label>
              </Checkbox.Item>
            </Checkbox.Main>

            <button
              disabled={
                checked.one && checked.two && checked.three ? false : true
              }
              className={
                checked.one && checked.two && checked.three ? "" : "disable"
              }
              onClick={() => handleSubmit()}
            >
              i understand
            </button>
          </div>
        </Overlay>
      )}
    </>
  );
};
export default Create;
