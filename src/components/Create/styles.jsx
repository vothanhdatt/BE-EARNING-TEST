import styled from "styled-components";

export const Overlay = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0px;
  background: #00000036;
  > div {
    position: fixed;
    width: 100%;
    left: 0;
    background: white;
    padding: 50px 0;
    bottom: 0;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    > p {
      font-weight: 700;
      font-size: 22px;
      line-height: 30px;

      color: #04004d;
    }
    > img {
      padding-top: 50px;
    }
    .down {
      margin-top: -50px;
      cursor: pointer;
    }
    > button {
      cursor: pointer;
      margin: 20px 0;
      width: 90%;
      height: 56px;
      border: none;
      background: #004dff;
      border-radius: 10px;
      font-weight: 700;
      font-size: 17px;
      line-height: 100%;
      /* identical to box height, or 17px */

      text-align: center;
      text-transform: uppercase;

      /* Color/Neutral/White */

      color: #ffffff;
    }
    .disable {
      background: #004dff8c;
    }
  }
`;
export const Checkbox = {
  Main: styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
    width: 90%;
    align-content: flex-start;
    margin: auto;
  `,
  Item: styled.div`
    display: flex;
    align-items: flex-start;
    text-align: left;
    gap: 10px;
    > label {
      color: #667386;
    }
    > input {
      &:after {
        top: 9px;
        left: 9px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
      }
    }
  `,
};
