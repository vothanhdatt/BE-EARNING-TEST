import styled from "styled-components";

export const Overlay = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0px;
  background: #00000036;
  > div {
    position: fixed;
    width: 100%;
    left: 0;
    background: white;
    padding: 50px 0;
    bottom: 0;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    > p {
      font-weight: 700;
      font-size: 22px;
      line-height: 30px;

      color: #04004d;
    }
    > img {
      padding-top: 50px;
    }
    .down {
      margin-top: -50px;
    }
  }
`;
