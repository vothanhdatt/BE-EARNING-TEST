import SVG from "components/SVG";
import { Overlay } from "./styles";
import { CountdownCircleTimer } from "react-countdown-circle-timer";

const Copied = ({ activeCopy, setActiveCopy }) => {
  return (
    <>
      {activeCopy && (
        <Overlay>
          <div>
            <div className="down">
              <SVG name="Down" />
            </div>

            <SVG name="Copied" />
            <p>Saved to clipboard</p>
            <div style={{ margin: "auto", width: "fit-content" }}>
              <CountdownCircleTimer
                size={30}
                strokeWidth={1}
                isPlaying
                duration={2}
                colors={["#A30000", "#A30000", "#F7B801", "#004777"]}
                colorsTime={[2, 1.5, 1, 0]}
              >
                {({ remainingTime }) => remainingTime}
              </CountdownCircleTimer>
            </div>
          </div>
        </Overlay>
      )}
    </>
  );
};
export default Copied;
